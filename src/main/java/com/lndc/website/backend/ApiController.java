package com.lndc.website.backend;

public final class ApiController {
    public static final String API_PREFIX               = "/api";
    public static final String API_REGISTRATION         = API_PREFIX + "/register";
    public static final String API_VERIFY_USER          = API_PREFIX + "/verify_user";
    public static final String API_LOGIN                = API_PREFIX + "/default_login";
    public static final String API_OAUTH_LOGIN          = API_PREFIX + "/oauth_login";
    public static final String API_LOGOUT               = API_PREFIX + "/logout";

    public static final String API_AUTHENTICATED_PREFIX = API_PREFIX + "/authenticated";
    public static final String API_USERS                = API_AUTHENTICATED_PREFIX + "/users";
}
